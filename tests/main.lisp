(defpackage cl-bio/tests/main
  (:use :cl
        :cl-bio
        :rove))
(in-package :cl-bio/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :cl-bio)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
