(defpackage cl-bio.fasta.sequence
  (:nicknames :bio.fasta.sequence)
  (:use :cl)
  (:import-from :bio.fasta
                ;;;for both
                #:get-seq
                #:find-seq
                #:find-repeat
                #:part-by-seq
                #:part-by-num
                #:insert-seq
                #:insert-num
                #:count-length
                ;;;for DNA sequence
                #:count-each-base
                #:reverse-seq
                #:complement-base
                #:complement-seq
                #:reverse-complement-seq
                #:translate-codon
                #:translate-seq
                ;;;for protein sequence
                #:count-each-residue
                #:count-charge
                #:residue-molecular-weight
                #:protein-molecular-weight
                )
  (:import-from :bio.util
                #:alist
                #:aget
                #:aset
                )
  (:export #:find-seq
           #:part-by-num
           #:count-length
           ))
(in-package :bio.fasta.sequence)

;;; fastaの配列を返す関数
(defun get-seq (fasta)
  "Get the sequence part of [fasta]."
  (second fasta))

;;; 配列の中で該当する配列の位置番号を返す関数
(defun find-seq (seq-line seq-find)
  "Find the position of [seq-find] in [seq-line], and return the position number of the first letter of [seq-find]."
  (search seq-find seq-line :test #'equal))

;;; 配列の中で該当する配列が何個あるか数える関数
(defun find-repeat (seq-line seq-find)
  "Count the repeat of [seq-find] in [seq-line]."
  (labels ((count-repeat (line fnd cnt)
             (if (null (find-seq line fnd))
                 cnt
                 (count-repeat (subseq line (+ (find-seq line fnd) (length fnd))) fnd (1+ cnt)))))
    (count-repeat seq-line seq-find 0)))

;;; 配列の始点と終点の部分配列を指定してその範囲の配列を返す関数
(defun part-by-seq (seq-line seq-start &optional seq-end)
  "Get the subsequence between [seq-start] and [seq-end], directed by sequence."
  (subseq seq-line
          (find-seq seq-line seq-start)
          (if (null seq-end)
              (find-seq seq-line seq-start)
              (+ (find-seq seq-line seq-end) (length seq-end)))))

;;; 配列の始点と終点の位置番号を指定してその範囲の配列を返す関数
(defun part-by-num (seq-line num-start &optional num-post-start)
  "Get the subsequence between [num-start] and [num-post-start] (not including the character at [num-post-start]), directed by position number."
  (subseq seq-line num-start num-post-start))

;;; 配列の始点と終点の部分配列を指定してその間に任意の挿入配列を挿入する関数
(defun insert-seq (seq-line insert pre-insert &optional post-insert)
  "Insert any sequence into any position of [seq-line], directed by sequence."
  (concatenate 'string
               (part-by-num seq-line 0 (+ (find-seq seq-line pre-insert) (length pre-insert)))
               insert
               (if (null post-insert)
                   (part-by-num seq-line (+ (find-seq seq-line pre-insert) (length pre-insert)))
                   (part-by-num seq-line (find-seq seq-line post-insert)))))

;;; 配列の始点と終点の位置番号を指定してその間に任意の挿入配列を挿入する関数
(defun insert-num (seq-line insert num-ins-start &optional num-post-start)
  "Insert any sequence into any position of [seq-line], directed by position number."
  (concatenate 'string
               (part-by-num seq-line 0 num-ins-start)
               insert
               (if (null num-post-start)
                   (part-by-num seq-line num-ins-start)
                   (part-by-num seq-line num-post-start))))

;;; 配列の長さを数える関数(配列の1文字のリスト形式と，1行の文字列形式に対応)
(defun count-length (seq)
  "Count the length of [seq] ([seq] should be either list of characters or 1-line-string."
  (length seq))

;;; 各塩基の数を数える関数
(defun count-each-base (seq-ls)
  "Count the number of Adenine, Guanine, Cytosine, Thymine in [seq-ls]; a characters-list of sequence."
  (let ((base-count (alist 'ade 0 'cit 0 'gua 0 'thy 0)))
    (dolist (base seq-ls)
      (cond ((char= base #\A) (aset 'ade (1+ (aget 'ade base-count)) base-count))
            ((char= base #\C) (aset 'cit (1+ (aget 'cit base-count)) base-count))
            ((char= base #\G) (aset 'gua (1+ (aget 'gua base-count)) base-count))
            ((char= base #\T) (aset 'thy (1+ (aget 'thy base-count)) base-count))))
    (alist 'A (aget 'ade base-count) 'C (aget 'cit base-count) 'G (aget 'gua base-count) 'T (aget 'thy base-count))))

;;; 塩基配列を逆から読むリスト/文字列を返す関数
(defun reverse-seq (seq)
  "Make a list/string of reversed sequence out of [seq]; either a list of characters or a string of sequence."
  (reverse seq))

;;; 入力された塩基に対して相補的な塩基を返す関数
(defun complement-base (base)
  "Return the complemental base of [base]."
  (cond ((char= base #\A) #\T)
        ((char= base #\T) #\A)
        ((char= base #\C) #\G)
        ((char= base #\G) #\C)
        ((char= base #\K) #\M)
        ((char= base #\M) #\K)
        ((char= base #\R) #\Y)
        ((char= base #\Y) #\R)
        ((char= base #\B) #\V)
        ((char= base #\V) #\B)
        ((char= base #\S) #\W)
        ((char= base #\W) #\S)
        ((char= base #\D) #\H)
        ((char= base #\H) #\D)
        (t base)))

;;; 塩基配列全体に対して相補的な塩基配列のリストを返す関数
(defun complement-seq (seq-ls)
  (reverse (reverse-complement-seq seq-ls)))

;;; 塩基配列全体に対して相補的な塩基配列を逆から読むリストを返す関数
(defun reverse-complement-seq (seq-ls)
  "Make a reversed list of complemental bases of [seq-ls]."
  (let ((comp-ls '()))
    (dolist (base seq-ls)
      (setf comp-ls (cons (complement-base base) comp-ls)))
    comp-ls))

;;; 3塩基の文字列を受け取ってコドンを対応するアミノ酸に翻訳する関数
(defun translate-codon (codon)
  "Translate the codon and return the suitable amino acid."
  (cond ((equal codon "TTT") #\F)
        ((equal codon "TTC") #\F)
        ((equal codon "TTT") #\L)
        ((equal codon "TTT") #\L)
        ((equal codon "TTA") #\L)
        ((equal codon "TTG") #\L)
        ((equal codon "CTT") #\L)
        ((equal codon "CTC") #\L)
        ((equal codon "CTA") #\L)
        ((equal codon "CTG") #\L)
        ((equal codon "ATT") #\I)
        ((equal codon "ATC") #\I)
        ((equal codon "ATA") #\I)
        ((equal codon "ATG") #\M)
        ((equal codon "GTT") #\V)
        ((equal codon "GTC") #\V)
        ((equal codon "GTA") #\V)
        ((equal codon "GTG") #\V)
        ((equal codon "TCT") #\S)
        ((equal codon "TCC") #\S)
        ((equal codon "TCA") #\S)
        ((equal codon "TCG") #\S)
        ((equal codon "CCT") #\P)
        ((equal codon "CCC") #\P)
        ((equal codon "CCA") #\P)
        ((equal codon "CCG") #\P)
        ((equal codon "ACT") #\T)
        ((equal codon "ACC") #\T)
        ((equal codon "ACA") #\T)
        ((equal codon "ACG") #\T)
        ((equal codon "GCT") #\A)
        ((equal codon "GCC") #\A)
        ((equal codon "GCA") #\A)
        ((equal codon "GCG") #\A)
        ((equal codon "TAT") #\Y)
        ((equal codon "TAC") #\Y)
        ((equal codon "TAA") #\*)
        ((equal codon "TAG") #\*)
        ((equal codon "CAT") #\H)
        ((equal codon "CAC") #\H)
        ((equal codon "CAA") #\Q)
        ((equal codon "CAG") #\Q)
        ((equal codon "AAT") #\N)
        ((equal codon "AAC") #\N)
        ((equal codon "AAA") #\K)
        ((equal codon "AAG") #\K)
        ((equal codon "GAT") #\D)
        ((equal codon "GAC") #\D)
        ((equal codon "GAA") #\E)
        ((equal codon "GAG") #\E)
        ((equal codon "TGT") #\C)
        ((equal codon "TGC") #\C)
        ((equal codon "TGA") #\*)
        ((equal codon "TGG") #\W)
        ((equal codon "CGT") #\R)
        ((equal codon "CGC") #\R)
        ((equal codon "CGA") #\R)
        ((equal codon "CGG") #\R)
        ((equal codon "AGT") #\S)
        ((equal codon "AGC") #\S)
        ((equal codon "AGA") #\R)
        ((equal codon "AGG") #\R)
        ((equal codon "GGT") #\G)
        ((equal codon "GGC") #\G)
        ((equal codon "GGA") #\G)
        ((equal codon "GGG") #\G)
        (t #\-)))

;;; 翻訳の始点を指定して終始コドンが現れるまでコドンを翻訳し、対応するアミノ酸の文字のリストを返す関数
(defun translate-seq (seq-line &optional num-start)
  "Translate codons into suitable amino acid, from [tl-start] till a stop codon appears."
  (do ((i (if num-start num-start (find-seq seq-line "ATG")) (+ i 3)) (aa-ls '()))
      ((> (+ i 3) (count-length seq-line)) (format t "Error: stop codon does not exist"))
      (setf aa-ls (cons (translate-codon (part-by-num seq-line i (+ i 3))) aa-ls))
      (when (char= (first aa-ls) #\*) (return-from translate-seq (reverse aa-ls)))))

;;; 各アミノ酸の数を数える関数
(defun count-each-residue (seq-ls)
  "Count the number of each residue in the protein."
  (let ((residue-ls (alist 'ala 0 'cys 0 'asp 0 'glu 0 'phe 0 'gly 0 'his 0 'ile 0 'lys 0 'leu 0 'met 0 'asn 0 'pro 0 'gln 0 'arg 0 'ser 0 'thr 0 'val 0 'trp 0 'tyr 0)))
    (dolist (residue seq-ls)
      (cond ((char= #\A residue) (aset 'ala (1+ (aget 'ala residue-ls)) residue-ls))
            ((char= #\C residue) (aset 'cys (1+ (aget 'cys residue-ls)) residue-ls))
            ((char= #\D residue) (aset 'asp (1+ (aget 'asp residue-ls)) residue-ls))
            ((char= #\E residue) (aset 'glu (1+ (aget 'glu residue-ls)) residue-ls))
            ((char= #\F residue) (aset 'phe (1+ (aget 'phe residue-ls)) residue-ls))
            ((char= #\G residue) (aset 'gly (1+ (aget 'gly residue-ls)) residue-ls))
            ((char= #\H residue) (aset 'his (1+ (aget 'his residue-ls)) residue-ls))
            ((char= #\I residue) (aset 'ile (1+ (aget 'ile residue-ls)) residue-ls))
            ((char= #\K residue) (aset 'lys (1+ (aget 'lys residue-ls)) residue-ls))
            ((char= #\L residue) (aset 'leu (1+ (aget 'leu residue-ls)) residue-ls))
            ((char= #\M residue) (aset 'met (1+ (aget 'met residue-ls)) residue-ls))
            ((char= #\N residue) (aset 'asn (1+ (aget 'asn residue-ls)) residue-ls))
            ((char= #\P residue) (aset 'pro (1+ (aget 'pro residue-ls)) residue-ls))
            ((char= #\Q residue) (aset 'gln (1+ (aget 'gln residue-ls)) residue-ls))
            ((char= #\R residue) (aset 'arg (1+ (aget 'arg residue-ls)) residue-ls))
            ((char= #\S residue) (aset 'ser (1+ (aget 'ser residue-ls)) residue-ls))
            ((char= #\T residue) (aset 'thr (1+ (aget 'thr residue-ls)) residue-ls))
            ((char= #\V residue) (aset 'val (1+ (aget 'val residue-ls)) residue-ls))
            ((char= #\W residue) (aset 'trp (1+ (aget 'trp residue-ls)) residue-ls))
            ((char= #\Y residue) (aset 'tyr (1+ (aget 'tyr residue-ls)) residue-ls))))
    (alist 'Ala (aget 'ala residue-ls)
           'Cys (aget 'cys residue-ls)
           'Asp (aget 'asp residue-ls)
           'Glu (aget 'glu residue-ls)
           'Phe (aget 'phe residue-ls)
           'Gly (aget 'gly residue-ls)
           'His (aget 'his residue-ls)
           'Ile (aget 'ile residue-ls)
           'Lys (aget 'lys residue-ls)
           'Leu (aget 'leu residue-ls)
           'Met (aget 'met residue-ls)
           'Asn (aget 'asn residue-ls)
           'Pro (aget 'pro residue-ls)
           'Gln (aget 'gln residue-ls)
           'Arg (aget 'arg residue-ls)
           'Ser (aget 'ser residue-ls)
           'Thr (aget 'thr residue-ls)
           'Val (aget 'val residue-ls)
           'Trp (aget 'trp residue-ls)
           'Tyr (aget 'tyr residue-ls))))

;;; タンパク質中にある電荷の数を数える関数
(defun count-charge (seq-ls)
  "Count the numbers of positively/negatively-charged amino acids and whole charge of the protein."
  (let ((charge-ls (alist 'count+ 0 'count- 0)))
    (dolist (residue seq-ls)
      (cond ((char= #\D residue) (aset 'count- (1+ (aget 'count- residue)) residue))
            ((char= #\E residue) (aset 'count- (1+ (aget 'count- residue)) residue))
            ((char= #\H residue) (aset 'count+ (1+ (aget 'count+ residue)) residue))
            ((char= #\K residue) (aset 'count+ (1+ (aget 'count+ residue)) residue))
            ((char= #\R residue) (aset 'count+ (1+ (aget 'count+ residue)) residue))))
    (alist '+ (aget 'count+ charge-ls)
           '- (aget 'count- charge-ls)
           'whole-protein (+ (aget 'count+ charge-ls) (aget 'count- charge-ls)))))

;;; アミノ酸残基を引数にとってその残基の分子量を返す関数
(defun residue-molecular-weight (residue)
  "Return the molecular weight of [residue]."
  (cond ((equal #\A residue) 71)
        ((equal #\C residue) 103)
        ((equal #\D residue) 115)
        ((equal #\E residue) 129)
        ((equal #\F residue) 147)
        ((equal #\G residue) 57)
        ((equal #\H residue) 157)
        ((equal #\I residue) 113)
        ((equal #\K residue) 128)
        ((equal #\L residue) 113)
        ((equal #\M residue) 131)
        ((equal #\N residue) 128)
        ((equal #\P residue) 97)
        ((equal #\Q residue) 128)
        ((equal #\R residue) 156)
        ((equal #\S residue) 87)
        ((equal #\T residue) 101)
        ((equal #\V residue) 99)
        ((equal #\W residue)186)
        ((equal #\Y residue) 163)))

;;; タンパク質の分子量を返す関数
(defun protein-molecular-weight (seq-ls)
  "Return the molecular weight of protein."
  (let ((mw 0))
    (dolist (residue seq-ls)
      (setf mw (+ (residue-molecular-weight residue) mw)))
    mw))

