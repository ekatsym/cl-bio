(defpackage cl-bio.fasta.header
  (:nicknames :bio.fasta.header)
  (:use cl)
  (:import-from :bio.fasta
                #:get-header
                ))
(in-package :bio.fasta.header)

;;; fastaのヘッダーを返す関数
(defun get-header (fasta)
  "Get the header part of [fasta]."
  (first fasta))

