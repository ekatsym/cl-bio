(defpackage cl-bio.fasta
  (:nicknames :bio.fasta)
  (:export
    ;;; io
    #:stream->string-ls
    #:string-ls->stream
    #:string-ls->line
    #:string-line->ls
    #:string->char-ls
    #:char-ls->string
    #:make-fasta
    ;;; sequence
    ;;; for both
    #:get-seq
    #:find-seq
    #:find-repeat
    #:part-by-seq
    #:part-by-num
    #:insert-seq
    #:insert-num
    #:count-length
    ;;; for DNA sequence
    #:count-each-base
    #:reverse-seq
    #:complement-base
    #:complement-seq
    #:reverse-complement-seq
    #:translate-codon
    #:translate-seq
    ;;; for protein sequence
    #:count-each-residue
    #:count-charge
    #:residue-molecular-weight
    #:protein-molecular-weight
    ;;; header
    #:get-header
    ))
