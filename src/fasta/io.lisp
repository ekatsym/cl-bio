(defpackage cl-bio.fasta.io
  (:nicknames :bio.fasta.io)
  (:use :cl)
  (:import-from :bio.fasta
                #:stream->string-ls
                #:string-ls->stream
                #:string-ls->line
                #:string-line->ls
                #:string->char-ls
                #:char-ls->string
                #:make-fasta)
  (:import-from :bio.fasta.sequence
                #:count-length
                #:part-by-num))
(in-package :bio.fasta.io)

;;; 入力ストリームを受け取り各行の文字列のリストを返す関数
(defun stream->string-ls (in-stream)
  "Read the [in-stream] and return a list of lines."
  (do ((line (read-line in-stream nil) (read-line in-stream nil)) (string-ls '() (cons line string-ls)))
      ((null line) (reverse string-ls))))

;;; 出力ストリームを受け取り各行の文字列のリストを出力ストリームにプリントする関数
(defun string-ls->stream (out-stream string-ls)
  "Write lines of string in [string-ls] on [out-stream]."
  (dolist (line string-ls) (format out-stream "~a~%" line)))

;;; 文字列のリストを1行の文字列に変換する関数
(defun string-ls->line (string-ls)
  "Make a 1-line-string out of [string-ls] by concatenating the lines."
  (let ((string-line ""))
    (dolist (line string-ls)
      (setf string-line (concatenate 'string string-line line)))
    string-line))

;;; 1行の文字列を60文字ごとに分けてリストにまとめる関数
(defun string-line->ls (string-line)
  "Make a list of strings with 60 characters out of [string-line]."
  (do ((string-ls '()) (i 0))
      ((>= (+ i 60) (count-length string-line)) (reverse (cons (part-by-num string-line i (count-length string-line)) string-ls)))
      (setf string-ls (cons (part-by-num string-line i (+ i 60)) string-ls) i (+ i 60) )))

;;; 文字列を文字のリストに変換する関数
(defun string->char-ls (string-line)
  "Make a list of characters of [string-line]."
  (coerce string-line 'list))

;;; 文字のリストを文字列に変換する関数
(defun char-ls->string (char-ls)
  "Make a string out of [char-ls]."
  (if (= 1 (count-length char-ls))
      (string (first char-ls))
      (coerce char-ls 'string)))

;;; ヘッダーと配列を1つのリストにまとめる関数
(defun make-fasta (header seq)
  "Make a list of ([header] [seq])."
  (list header seq))

