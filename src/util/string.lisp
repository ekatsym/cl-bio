(in-package :cl-bio.util)

(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

(defun reread (&rest args)
  (identity (read-from-string (apply #'mkstr args))))
