(in-package :cl-bio.util)

;;; standard closure
(defmacro closure (bindings args &body body)
  `(let ,bindings (lambda ,args ,@body)))

(defmacro defclosure (name bindings args &body body)
  `(let ,bindings (defun ,name ,args ,@body)))

(defmacro meta-closure (init-args bindings args &body body)
  `(lambda ,init-args
     (closure ,bindings ,args ,@body)))

(defmacro define-meta-closure (name init-args bindings args &body body)
  `(defun ,name ,init-args
     (closure ,bindings ,args ,@body)))

;;; open closure
(defmacro! open-closure (bindings &body clauses)
  (flet ((binding-format (binding)
           (cond ((atom binding)
                  `(,binding nil))
                 ((n-list? 1 binding)
                  `(,@binding nil))
                 (t
                  binding))))
    (let ((bindings (mapcar #'binding-format bindings)))
      `(let ((self nil))
         (let ,bindings
           (setq self
                 (dlambda
                   ,@clauses
                   (:get (,g!var)
                    (ecase ,g!var
                      ,@(mapcar (lambda (binding)
                                  `(,(first binding) ,(first binding)))
                                bindings)))
                   (:set (,g!var ,g!new)
                    (ecase ,g!var
                      ,@(mapcar (lambda (binding)
                                  `(,(first binding) (setq ,(first binding) ,g!new)))
                                bindings))))))
         self))))

(defmacro! open-closure* (bindings &body clauses)
  (flet ((binding-format (binding)
           (cond ((atom binding)
                  `(,binding nil))
                 ((n-list? 1 binding)
                  `(,@binding nil))
                 (t
                  binding))))
    (let ((bindings (mapcar #'binding-format bindings)))
      `(let ((self nil))
         (let ,bindings
           (setq self
                 (dlambda
                   ,@clauses
                   (:get (,g!var)
                    (ecase ,g!var
                      ,@(mapcar (lambda (binding)
                                  `(,(first binding) ,(first binding)))
                                bindings)
                      (self self)))
                   (:set (,g!var ,g!new)
                    (ecase ,g!var
                      ,@(mapcar (lambda (binding)
                                  `(,(first binding) (setq ,(first binding) ,g!new)))
                                bindings)
                      (self (setq self ,g!new)))))))
         (lambda (&rest ,g!args) (apply self ,g!args))))))

(defmacro! define-open-closure (name bindings &body clauses)
  (flet ((binding-format (binding)
           (cond ((atom binding)
                  `(,binding nil))
                 ((n-list? 1 binding)
                  `(,@binding nil))
                 (t
                  binding))))
    (let ((bindings (mapcar #'binding-format bindings)))
      `(let ((self nil))
         (let ,bindings
           (ddefun ,name
             ,@clauses
             (:get (,g!var)
              (ecase ,g!var
                ,@(mapcar (lambda (binding)
                            `(,(first binding) ,(first binding)))
                          bindings)))
             (:set (,g!var ,g!new)
              (ecase ,g!var
                ,@(mapcar (lambda (binding)
                            `(,(first binding) (setq ,(first binding) ,g!new)))
                          bindings))))
           (setq self #',name))
         ',name))))

(defmacro! define-open-closure* (name bindings &body clauses)
  (flet ((binding-format (binding)
           (cond ((atom binding)
                  `(,binding nil))
                 ((n-list? 1 binding)
                  `(,@binding nil))
                 (t
                  binding))))
    (let ((bindings (mapcar #'binding-format bindings)))
      `(let ((self nil))
         (let ,bindings
           (setq self
                 (dlambda
                   ,@clauses
                   (:get (,g!var)
                    (ecase ,g!var
                      ,@(mapcar (lambda (binding)
                                  `(,(first binding) ,(first binding)))
                                bindings)
                      (self self)))
                   (:set (,g!var ,g!new)
                    (ecase ,g!var
                      ,@(mapcar (lambda (binding)
                                  `(,(first binding) (setq ,(first binding) ,g!new)))
                                bindings)
                      (self (setq self ,g!new)))))))
         (defun ,name (&rest ,g!args)
           (apply self ,g!args))))))

(declaim (inline get-open-closure))
(defun get-open-closure (open-closure key)
  (funcall open-closure :get key))
(defsetf get-open-closure (open-closure key) (new)
         `(progn (funcall ,open-closure :set ,key ,new) ,new))

(defmacro with-open-closure (vars open-closure &body body)
  `(let ,(mapcar (lambda (var)
                   (cond ((atom var)
                          `(,var (get-open-closure ,open-closure ',var)))
                         ((n-list? 1 var)
                          `(,(first var) (get-open-closure ,open-closure ',(first var))))
                         ((n-list? 2 var)
                          `(,(first var) (get-open-closure ,open-closure ',(second var))))
                         (t
                          (error "~a~%is an invalid form for WITH-OPEN-CLOSURE bindings."
                                 vars))))
                 vars)
     ,@body))

(defmacro with-open-closure* (vars open-closure &body body)
  `(symbol-macrolet ,(mapcar (lambda (var)
                               (cond ((atom var)
                                      `(,var (get-open-closure ,open-closure ',var)))
                                     ((n-list? 1 var)
                                      `(,(first var) (get-open-closure ,open-closure ',(first var))))
                                     ((n-list? 2 var)
                                      `(,(first var) (get-open-closure ,open-closure ',(second var))))
                                     (t
                                      (error "~a~%is an invalid form for WITH-OPEN-CLOSURE* bindings."
                                             vars))))
                             vars)
     ,@body))

(defmacro meta-open-closure (init-args bindings &body clauses)
  `(lambda ,init-args
     (open-closure ,bindings ,@clauses)))

(defmacro meta-open-closure* (init-args bindings &body clauses)
  `(lambda ,init-args
     (open-closure* ,bindings ,@clauses)))

(defmacro define-meta-open-closure (name init-args bindings &body clauses)
  `(defun ,name ,init-args
     (open-closure ,bindings ,@clauses)))

(defmacro define-meta-open-closure* (name init-args bindings &body clauses)
  `(defun ,name ,init-args
     (open-closure* ,bindings ,@clauses)))
