(in-package :cl-bio.util)

(defmacro! nondeterministic-let (bindings &body body)
  (let ((gs (gensyms (length bindings)))
        (vars (mapcar #'first bindings))
        (lst (mapcar #'rest bindings)))
    `(nondeterministic-let*
       ,(mapcar (lambda (g l) `(,g ,@l))
                gs lst)
       (let ,(mapcar #'list vars gs) ,@body))))

(defmacro! nondeterministic-let* (bindings &body body)
  (labels ((rec (bindings body)
             (if (endp bindings)
                 `(push (progn ,@body) ,g!result)
                 (destructuring-bind ((var lst) &rest rest-bindings) bindings
                   `(dolist (,var (remove-duplicates ,lst))
                      ,(rec rest-bindings body))))))
    `(let ((,g!result '()))
       ,(rec bindings body)
       (remove-duplicates ,g!result))))

(defmacro nondeterministic-let/filter (bindings &body body)
  (let ((gs (gensyms (length bindings)))
        (vars (mapcar #'first bindings))
        (lst (mapcar #'rest bindings)))
    `(nondeterministic-let*/filter
       ,(mapcar (lambda (g l) `(,g ,@l))
                gs lst)
       (let ,(mapcar #'list vars gs) ,@body))))

(defmacro! nondeterministic-let*/filter (bindings &body body)
  (labels ((rec (bindings body)
             (if (endp bindings)
                 `(awhen (progn ,@body)
                    (push it ,g!result))
                 (destructuring-bind ((var lst) &rest rest-bindings) bindings
                   `(dolist (,var (remove-duplicates ,lst))
                      ,(rec rest-bindings body))))))
    `(let ((,g!result '()))
       ,(rec bindings body)
       (remove-duplicates ,g!result))))
