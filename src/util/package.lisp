(defpackage cl-bio.util
  (:nicknames :bio.util)
  (:use :cl)
  (:export
    ;; number
    #:positive
    #:non-positive
    #:negative
    #:non-negative
    #:natural
    #:index

    ;; list
    #:proper-list #:proper-list?
    #:constant-nil #:xcons #:consn #:iterate #:enumerate
    #:take #:drop #:sublist
    #:last1 #:append1 #:conc1 #:force-list
    #:n-list?
    #:length= #:length<= #:length<
    #:group #:flatten
    #:find2 #:before #:after #:duplicate
    #:split2 #:split2-if #:splitn #:splitn-if #:split #:split-if
    #:most #:best #:mostn
    #:mappend #:filter #:mapcars
    #:rmapcar #:prune #:prune-if
    #:rotate #:nrotate #:filter

    ;; io
    #:read-list
    #:read-stream #:read-file
    #:prompt #:break-loop

    ;; symbol
    #:symbolicate
    #:gensyms
    #:explode

    ;; string
    #:mkstr
    #:reread

    ;; function
    #:memoize
    #:compose #:ncompose
    #:partial #:rpartial
    #:curry #:rcurry
    #:fn-if #:fn-and #:fn-or
    #:recursion #:tail-recursion
    #:tree-traverse #:tree-recursion

    ;; defmacro
    #:defmacro!

    ;; macro
    #:cons-let #:cons-let*
    #:if-let #:when-let #:unless-let
    #:cond-let
    #:switch
    #:if3 #:nif #:xor
    #:in #:inq #:in-if
    #:while #:till #:for
    #:do-tuples/open
    #:do-tuples/close

    ;; array
    #:map-array
    #:do-array

    ;; multiple value
    #:multiple-value-let
    #:multiple-value-let*
    #:multiple-value-do
    #:multiple-value-do*

    ;; anaphora
    #:it
    #:alet
    #:aand #:aor
    #:aif #:acond
    #:awhen #:aunless
    #:acase #:aecase #:accase
    #:atypecase #:aetypecase #:actypecase
    #:self #:alambda

    ;; dispatch function
    #:dlambda #:ddefun #:dflet #:dlabels

    ;; closure
    #:closure #:defclosure #:meta-closure #:define-meta-closure
    ;#:self
    #:open-closure #:open-closure*
    #:define-open-closure #:define-open-closure*
    #:meta-open-closure #:meta-open-closure*
    #:define-meta-open-closure #:define-meta-open-closure*
    #:get-open-closure #:with-open-closure #:with-open-closure*

    ;; lazy
    #:promise #:promise? #:delay #:force

    ;; continuation
    #:continuous #:multiple-value-continuous

    ;; nondeterministic computation
    #:nondeterministic-let #:nondeterministic-let*
    #:nondeterministic-let/filter #:nondeterministic-let*/filter

    ;; monad

    ;; alist
    #:alist #:alist?
    #:aget #:aset #:arem
    #:amap))
