(in-package :cl-bio.util)

(defmacro multiple-value-let* (bindings &body body)
  (if (null bindings)
      `(progn ,@body)
      (destructuring-bind (binding . rest-bindings) bindings
        (if (or (atom binding) (n-list? 1 binding))
            `(multiple-value-bind ,(force-list binding) nil
               (multiple-value-let* ,rest-bindings ,@body))
            `(multiple-value-bind ,(force-list (first binding)) ,(second binding)
               (multiple-value-let* ,rest-bindings ,@body))))))

(defmacro multiple-value-let (bindings &body body)
  (let ((syms (mapcar (lambda (binding)
                        (mapcar (compose #'gensym #'string)
                                (force-list (first (force-list binding)))))
                      bindings)))
    `(multiple-value-let* ,(mapcar (lambda (binding sym)
                                     (if (or (atom binding) (n-list? 1 binding))
                                         `(,sym nil)
                                         `(,sym ,(second binding))))
                                   bindings syms)
       (multiple-value-let* ,(mapcar (lambda (binding sym)
                                       `(,(first (force-list binding))
                                          ,(if (atom sym)
                                               sym
                                               `(values ,@sym))))
                                     bindings syms)
         ,@body))))

(defmacro! multiple-value-do* (varlist (end &rest result) &body body)
  `(block
     nil
     (multiple-value-let*
       ,(mapcar (partial #'take 2) varlist)
       (tagbody
         (go ,g!loop)
         ,g!loop
         (tagbody ,@body)
         (setf ,@(mappend (lambda (clause)
                            (ecase (length clause)
                              (2 '())
                              (3 (destructuring-bind (var _ next) clause
                                   (declare (ignore _))
                                   (if (atom var)
                                       `(,var ,next)
                                       `((values ,@var) ,next))))))
                          varlist))
         ,g!end
         (unless ,end (go ,g!loop))
         (return (progn ,@result))))))

(defmacro! multiple-value-do (varlist (end &rest result) &body body)
  `(block
     nil
     (multiple-value-let
       ,(mapcar (partial #'take 2) varlist)
       (tagbody
         (go ,g!loop)
         ,g!loop
         (tagbody ,@body)
         (psetf ,@(mappend (lambda (clause)
                             (ecase (length clause)
                               (2 '())
                               (3 (destructuring-bind (var _ next) clause
                                    (declare (ignore _))
                                    (if (atom var)
                                        `(,var ,next)
                                        `((values ,@var) ,next))))))
                           varlist))
         ,g!end
         (unless ,end (go ,g!loop))
         (return (progn ,@result))))))

(defun multiple-value-compose (&rest functions)
  (labels ((rec (fns acc)
             (declare (optimize (speed 3))
                      (type function acc))
             (if (endp fns)
                 acc
                 (rec (rest fns)
                      (lambda (&rest args)
                        (multiple-value-call acc (apply (the function (first fns)) args)))))))
    (if functions
        (rec (rest functions) (first functions))
        #'values)))
