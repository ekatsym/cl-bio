(in-package :cl-bio.util)

;;; let
(defmacro cons-let (bindings &body body)
  (let ((syms (gensyms (length bindings))))
    `(let ,(mapcar (lambda (sym binding)
                     `(,sym ,(second binding)))
                   syms bindings)
       (let ,(mappend (lambda (binding sym)
                        (destructuring-bind ((car cdr) cons) binding
                          (declare (ignore cons))
                          `((,car (car ,sym))
                            (,cdr (cdr ,sym)))))
                      bindings syms)
         ,@body))))

(defmacro cons-let* (bindings &body body)
  (if (null bindings)
      `(progn ,@body)
      (destructuring-bind (((car1 cdr1) cons1) . rest-bindings) bindings
        `(cons-let (((,car1 ,cdr1) ,cons1))
           (cons-let* ,rest-bindings ,@body)))))

(defmacro if-let ((var val) then &optional else)
  `(let ((,var ,val))
     (if ,var ,then ,else)))

(defmacro when-let ((var val) &body body)
  `(let ((,var ,val))
     (when ,var ,@body)))

(defmacro unless-let ((var val) &body body)
  `(let ((,var ,val))
     (unless ,var ,@body)))

(defmacro cond-let (clauses &body body)
  (let ((vars (remove-duplicates
                (mappend (lambda (clause)
                           (mapcar #'first (rest clause)))
                         clauses))))
    `(let ,vars
       (declare (ignorable ,@vars))
       (cond ,@(mapcar (lambda (clause)
                         (destructuring-bind (test . then) clause
                           `(,test (let ,then ,@body))))
                       clauses)))))

;;; condition
(defmacro! if3 (o!test t-then nil-then else)
  `(cond ((eq ,g!test t) ,t-then)
         ((eq ,g!test nil) ,nil-then)
         (t ,else)))

(defmacro! nif (o!test positive zero negative)
  `(cond ((plusp ,g!test) ,positive)
         ((zerop ,g!test) ,zero)
         ((minusp ,g!test) ,negative)))

(defmacro xor (&rest forms)
  (if (null forms)
      'nil
      `(if ,(first forms)
           (not (and ,@(rest forms)))
           (or ,@(rest forms)))))

(defmacro! in (o!x &rest choices)
  `(or ,@(mapcar (lambda (c) `(eql ,g!x ,c))
                 choices)))

(defmacro! inq (o!x &rest choices)
  `(or ,@(mapcar (lambda (c) `(eq ,g!x ',c)) choices)))

(defmacro! in-if (o!fn &rest choices)
  `(or ,@(mapcar (lambda (c) `(funcall ,g!fn ,c))
                 choices)))

(defmacro! switch ((o!keyform &key test) &body clauses)
  `(cond ,@(mapcar (lambda (clause)
                     (destructuring-bind (test-form . then-form) clause
                       (if (eq test-form 'otherwise)
                           `(t ,@then-form)
                           (if test
                               (if (and (n-list? 2 test)
                                        (symbolp (second test))
                                        (inq (first test) quote function))
                                   `((,(second test) ,g!keyform ,test-form)
                                     ,@then-form)
                                   `((funcall ,test ,g!keyform ,test-form)
                                     ,@then-form))
                               `((eql ,g!keyform ,test-form))))))
                   clauses)))

;;; loop macro
(defmacro while (test &body body)
  `(do ()
       ((not ,test))
       ,@body))

(defmacro till (test &body body)
  `(do ()
       (,test)
       ,@body))

(defmacro! for ((var start end) &body body)
  `(do ((,var ,start (1+ ,var))
        (,g!limit ,end))
       ((> ,var ,g!limit))
       ,@body))

(defmacro! do-tuples/open ((vars o!list &optional (result)) &body body)
  `(progn
     (mapc (lambda ,vars ,@body)
           ,@(enumerate (length vars)
                        :fn (lambda (n) `(drop ,n ,g!list))))
     ,result))

(defmacro! do-tuples/close ((vars o!list &optional (result)) &body body)
  `(progn
     (mapc (lambda ,vars ,@body)
           ,@(enumerate (length vars)
                        :fn (lambda (n) `(nrotate ,n ,g!list))))
     ,result))
