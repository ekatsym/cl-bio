(in-package :cl-bio.util)

(defmacro continuous (var receiver &body body)
  `(funcall ,receiver (lambda (,var) ,@body)))

(defmacro multiple-value-continuous (var receiver &body body)
  `(funcall ,receiver (lambda (&rest ,var) ,@(subst `(apply #'values ,var) var body))))
