(in-package :cl-bio.util)

(defun read-list (&optional (stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  (identity
    (read-from-string
      (concatenate 'string "(" (read-line stream eof-error-p eof-value recursive-p) ")"))))

(defun read-stream (stream)
  (let ((newline (string #\newline)))
    (reduce (lambda (x y) (concatenate 'string y newline x))
            (loop :for line := (read-line stream nil nil)
                  :while line
                  :collect line))))

(defun read-file (filespec)
  (with-open-file (s filespec
                     :direction :input
                     :if-does-not-exist :error)
    (read-stream s)))

(defun prompt (control-string &rest format-arguments)
  (apply #'format *query-io* control-string format-arguments)
  (read *query-io*))

(defun break-loop (eval quit control-string &rest format-arguments)
  (format *query-io* "Entering breaking-loop.~%")
  (loop
    (let ((in (apply #'prompt control-string format-arguments)))
      (if (funcall quit in)
          (return)
          (format *query-io* "~a~%" (funcall eval in))))))
