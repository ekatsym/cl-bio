(in-package :cl-bio.util)

(defmacro defmacro/g! (name args &body body)
  (flet ((flatten-defmacro! (x)
           (labels ((rec (x acc)
                      (cond ((null x)
                             acc)
                            #+sbcl ((sb-impl::comma-p x)
                                    (rec (sb-impl::comma-expr x) acc))
                            ((atom x)
                             (cons x acc))
                            (t
                             (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))

         (g!-symbol? (sym)
           (and (symbolp sym)
                (> (length (symbol-name sym)) 2)
                (string= (symbol-name sym)
                         "G!"
                         :start1 0 :end1 2))))

    (let ((syms (remove-duplicates
                  (remove-if-not #'g!-symbol?
                                 (flatten-defmacro! body)))))
      `(defmacro ,name ,args
         (let , (mapcar
                 (lambda (s)
                   `(,s (gensym ,(subseq
                                   (symbol-name s)
                                   2))))
                 syms)
           ,@body)))))

(defmacro defmacro! (name lambda-list &body body)
  (flet ((flatten-defmacro! (x)
           (labels ((rec (x acc)
                      (cond ((null x)
                             acc)
                            #+sbcl ((sb-impl::comma-p x)
                                    (rec (sb-impl::comma-expr x) acc))
                            ((atom x)
                             (cons x acc))
                            (t
                             (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))

         (o!-symbol? (sym)
           (and (symbolp sym)
                (> (length (symbol-name sym)) 2)
                (string= (symbol-name sym)
                         "O!"
                         :start1 0 :end1 2)))

         (o!-symbol->g!-symbol (sym)
           (intern (concatenate 'string "G!" (subseq (symbol-name sym) 2)))))

    (let* ((os (remove-if-not #'o!-symbol?
                              (flatten-defmacro! lambda-list)))
           (gs (mapcar #'o!-symbol->g!-symbol os)))
      (if (null os)
        `(defmacro/g! ,name ,lambda-list ,@body)
        `(defmacro/g! ,name ,lambda-list
           `(let ,(mapcar #'list (list ,@gs) (list ,@os))
              ,(progn ,@body)))))))
