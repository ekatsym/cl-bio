(defsystem "cl-bio"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :components ((:module "src"
                :serial t
                :components
                ((:module "util"
                  :components
                  ((:file "package")
                   (:file "number")
                   (:file "list")
                   (:file "io")
                   (:file "symbol")
                   (:file "string")
                   (:file "function")
                   (:file "defmacro")
                   (:file "macro")
                   (:file "array")
                   (:file "multiple-value")
                   (:file "anaphora")
                   (:file "dispatch")
                   (:file "closure")
                   (:file "lazy")
                   (:file "continuation")
                   (:file "nondeterminism")
                   (:file "alist")))
                 (:module "fasta"
                  :serial t
                  :components
                  ((:file "package")
                   (:file "sequence")
                   (:file "io")
                   (:file "header")))
                 (:file "package"))))
  :description ""
  :in-order-to ((test-op (test-op "cl-bio/tests"))))

(defsystem "cl-bio/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("cl-bio"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cl-bio"
  :perform (test-op (op c) (symbol-call :rove :run c)))
